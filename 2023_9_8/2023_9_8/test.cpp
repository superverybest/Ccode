class Solution {
public:

    vector<int>all;
    bool isUnivalTree(TreeNode* root)
    {
        if (root == nullptr)
            return true;
        int val = root->val;
        int i = 0;
        return isonce(root, &val, &i);

    }

    bool isonce(TreeNode*& root, int* val, int* i)
    {
        if (root == nullptr)return true;
        if (root->val != *val)(*i)++;
        if ((*i) != 0)return false;


        return isonce(root->left, val, i) && isonce(root->right, val, i);

    }
};

void PreOrder(BTNode* root)//前序遍历 
{
    printf("%c ", root->data);//打印当前节点的值data 
    PreOrder(root->left);//对左子树进行遍历 
    PreOrder(root->right);//对右子树进行遍历 
}

void InOrder(BTNode* root)//中序遍历 
{
    InOrder(root->left);//对左子树进行遍历 
    printf("%c ", root->data);//打印当前节点的值data 
    InOrder(root->right);//对右子树进行遍历 
}

void PastOrder(BTNode* root)//后序遍历 
{
    PastOrder(root->left);//对左子树进行遍历 
    PastOrder(root->right);//对右子树进行遍历 
    printf("%c ", root->data);//打印当前节点的值data 
}

class Solution {
public:
    int maxDepth(TreeNode* root)
    {
        if (root == nullptr)
            return 0;

        int leftheight = maxDepth(root->left);
        int rightheight = maxDepth(root->right);

        return leftheight > rightheight ? leftheight + 1 : rightheight + 1;

    }

};

class Solution {
public:
    TreeNode* invertTree(TreeNode* root)
    {
        if (root == nullptr)
            return nullptr;
        swap(root->left, root->right);
        invertTree(root->left);
        invertTree(root->right);
        return root;
    }
};

class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q)
    {
        if (p == nullptr && q != nullptr)
            return false;
        if (p != nullptr && q == nullptr)
            return false;
        if (p == nullptr && q == nullptr)
            return true;
        if (p->val != q->val)
            return false;
        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
    }
};