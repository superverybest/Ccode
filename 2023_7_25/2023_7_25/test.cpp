/*
leetcode102
leetcode107
*/

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        vector<vector<int>> res;
//        queue<TreeNode*> myqueue;
//        if (root)myqueue.push(root);
//        while (myqueue.size())
//        {
//            vector<int>level;
//            int len = myqueue.size();
//            while (len--)
//            {
//                auto t = myqueue.front();
//                myqueue.pop();
//                level.push_back(t->val);
//                if (t->left)myqueue.push(t->left);
//                if (t->right)myqueue.push(t->right);
//            }
//            res.push_back(level);
//        }
//
//        return res;
//
//    }
//};

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//        vector<vector<int>> res;
//        queue<TreeNode*> myqueue;
//        if (root)myqueue.push(root);
//        while (myqueue.size())
//        {
//            vector<int> level;
//            int len = myqueue.size();
//            while (len--)
//            {
//                auto t = myqueue.front();
//                myqueue.pop();
//                level.push_back(t->val);
//                if (t->left)myqueue.push(t->left);
//                if (t->right)myqueue.push(t->right);
//            }
//            res.push_back(level);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};