#define CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <vector>
//#include <limits.h>
//#include <math.h>
//using namespace std;
//
//void nums(int n, vector<int>& a)
//{
//    int i = 2;
//    for (i = 2;i <= sqrt(n);i++)
//    {
//        if (n % i == 0)
//        {
//            a.push_back(i);
//            if (n / i != i)
//                a.push_back(n / i);
//        }
//    }
//}
//
//void dp(int n, int m)
//{
//    vector<int> step(m + 1, INT_MAX);
//
//    step[n] = 0;
//
//    for (int i = n;i < m;i++)
//    {
//        if (step[i] == INT_MAX)
//            continue;
//
//        vector<int> a;
//        nums(i, a);
//
//        for (int j = 0;j < a.size();j++)
//        {
//            if (a[j] + i <= m && step[a[j] + i] != INT_MAX)
//            {
//                step[a[j] + i] = step[a[j] + i] < step[i] + 1 ? step[a[j] + i] : step[i] + 1;
//            }
//            else if (a[j] + i <= m)
//            {
//                step[a[j] + i] = step[i] + 1;
//            }
//        }
//    }
//    cout << (step[m] == INT_MAX ? -1 : step[m]) << endl;
//}
//
//int main()
//{
//    int n, m;
//    while (cin >> n >> m)
//    {
//        dp(n, m);
//    }
//    return 0;
//}

#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    string s;
    getline(cin, s);

    vector<string> svec;

    bool isstr = false;

    string tmp = "";

    for (auto e : s)
    {
        if (e == ' ')
        {
            if (!isstr && tmp != "")
            {
                svec.push_back(tmp);
                tmp = "";
            }
            else
            {
                if(isstr)
                tmp.push_back(e);
            }
        }
        else if (e == '"')
        {
            isstr = !isstr;
            if (!isstr)
            {
                svec.push_back(tmp);
                tmp = "";
            }
        }
        else
        {
            tmp.push_back(e);
        }
    }
    svec.push_back(tmp);
    cout << svec.size() << endl;
    for (auto e : svec)
    {
        cout << e << endl;
    }
    return 0;
}