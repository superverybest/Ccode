#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

int main() {
    char arc[] = "http://c.biancheng.net/cplus/11/";
    int i;
    for (i = 0; i < strlen(arc); i++) {
        cout << arc[i];
    }
    cout << endl;

    vector<char>myvector(arc, arc + 23);
    vector<char>::iterator iter;
    for (iter = myvector.begin(); iter != myvector.end(); ++iter) {
        cout << *iter;
    }
    return 0;
}