#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main() {
    int a, b;
    int i = 0;
    scanf("%d%d", &a, &b);
    if (b > a)
    {
        b = b ^ a;
        a = b ^ a;
        b = b ^ a;
    }
    for (i = a;;i++)
    {
        if (i % a == 0 && i % b == 0)
            break;
    }
    printf("%d", i);
    return 0;
}