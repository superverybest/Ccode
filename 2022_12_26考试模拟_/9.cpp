#include <stdio.h>

int main( )
{
	int arr[2][10];
	int i=0;
	int j=0;
	for(i=0;i<2;i++)
	{
		for(j=0;j<10;j++)
		{
			if(i==0&&j==0||i==0&&j==1)
			{
				arr[i][j]=1;
			}
			else if(i==1&&j==0)
			{
				arr[i][j]=arr[i-1][9]+arr[i-1][8];
			}
			else if(i==1&&j==1)
			{
				arr[i][j]=arr[i-1][9]+arr[i][0];
			}
			else
			{
				arr[i][j]=arr[i][j-1]+arr[i][j-2];
			}
		}
	}
	for(i=0;i<2;i++)
	{
		for(j=0;j<10;j++)
		{
			printf("%5d",arr[i][j]);
		}
		printf("\n");
	}
    return 0;
}

