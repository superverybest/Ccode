#include <stdio.h>
double fact(int n);
int main( )
{
    int i,n;
    double s=0;
    scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		s+=(1/fact(i));	
	}
    printf("%.5f\n",s);
    return 0;
}
double fact(int n)
{
	double res=1;
	while(n>0)
	{
		res*=n--;
	}
	return res;	
}
