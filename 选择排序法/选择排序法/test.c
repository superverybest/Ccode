#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
void sort(int* a, int n)
{
	int i = 0;
	int j = 0;
	for (i = 0;i < n;i++)
	{
		int min = i;
		for (j = i + 1;j < n;j++)
		{
			if (a[j] < a[min])
			{
				min = j;
			}
		}
		int tmp = a[i];
		a[i] = a[min];
		a[min] = tmp;
	}
}
int main()
{
	int a[10];
	int n = 0;
	int i = 0;
	scanf("%d", &n);
	for (i = 0;i < n;i++)
	{
		scanf("%d", &a[i]);
	}
	sort(a, n);
	for (i = 0;i < n;i++)
	{
		printf("%d ", a[i]);
	}
	return 0;
}