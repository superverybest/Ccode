#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int Fin(int n)
{
    if (n <= 2)
        return n;
    else
    {
        return Fin(n - 1) + Fin(n - 2);
    }
}
int main()
{
    int ret = 0;
    int n = 0;
    scanf("%d", &n);
    ret = Fin(n);
    printf("%d", ret);
    return 0;
}