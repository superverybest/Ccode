#define _CRT_SECURE_NO_WARNINGS 1

//
//#include <iostream>
//using namespace std;
//
//char getfirstonechar(const string str)
//{
//    for (int i = 0;i < str.size();++i)
//    {
//        int index1 = str.find(str[i]);
//        int index2 = str.rfind(str[i]);
//        if (index1 == index2)
//            return str[i];
//    }
//    return -1;
//}
//
//int main()
//{
//    string in;
//
//    cin >> in;
//
//    char hash[255] = { 0 };
//
//    for (int i=0;i<in.size();++i)
//    {
//        hash[in[i]]++;
//    }
//
//    for (int i = 0;i < 255;++i)
//    {
//        if (hash[i] == 1)
//            cout << in[i];
//    }
//
//
//
//
//    return 0;
//}

#include <iostream>
using namespace std;

int GCD(int first, int second)
{
    int temp;
    while (temp = first % second)
    {
        first = second;
        second = temp;
    }
    return second;
}

int battle(int n, int init)
{
    int level[n];

    for (int i = 0;i < n;++i)
    {
        cin >> level[i];
    }

    for (int i = 0;i < n;++i)
    {
        if (init >= level[i])
        {
            init = init + level[i];
        }
        else if (init < level[i])
        {
            init = init + GCD(init, level[i]);
        }
    }

    return init;
}

int main()
{
    int num, init;
    while (cin >> num >> init)
    {
        cout << battle(num, init) << endl;
    }
    return 0;
}