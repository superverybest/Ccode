#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;
#include <iostream>

namespace superverybest
{

	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			, _pcount(new int(1))
		{}

		~shared_ptr()
		{
			if (--(*_pcount) == 0)
			{
				cout << "delete:" << _ptr << endl;
				delete _ptr;
				delete _pcount;
			}
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			, _pcount(sp._pcount)
		{
			++(*_pcount);
		}

		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (_ptr == sp._ptr)
				return *this;

			if (--(*_pcount) == 0)
			{
				delete _ptr;
				delete _pcount;
			}

			_ptr = sp._ptr;
			_pcount = sp._pcount;
			++(*_pcount);

			return *this;
		}

		int use_count()const
		{
			return *_pcount;
		}

		T* get()const
		{
			return _ptr;
		}

	private:
		T* _ptr;
		int* _pcount;

	};
}

int main()
{
	superverybest::shared_ptr<int> test0(new int(0));
	superverybest::shared_ptr<int> test1(new int(100));
	superverybest::shared_ptr<int> test2(new int(50));
	superverybest::shared_ptr<int> test3(new int(1));
	cout << "test0:" << test0.get() << endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	//cout << "test4:" << test4.get() << endl;
	cout<<test1.use_count()<<endl;
	test1=test0;
	cout << "test0:" << test0.get() << endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	cout << test1.use_count() << endl;
	test1 = test2;
	cout << "test0:" << test0.get() << endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	cout << test1.use_count() << endl;
	test1 = test3;
	cout << "test0:" << test0.get() << endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	cout << test1.use_count() << endl;

	superverybest::shared_ptr<int> test4(test1);
	cout << "test0:" << test0.get() << endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	cout << "test4:" << test4.get() << endl;
	cout << test1.use_count() << endl;
	cout << test4.use_count() << endl;

	cout << "test0:"<<test0.get() <<endl;
	cout << "test1:" << test1.get() << endl;
	cout << "test2:" << test2.get() << endl;
	cout << "test3:" << test3.get() << endl;
	cout << "test4:" << test4.get() << endl<<endl;

	return 0;
}