#include <stdio.h>

int main() {
    int year = 0;
    int month = 0;
    int day = 0;
    int res = 0;
    int is_leap_year = 0;
    scanf("%d %d %d", &year, &month, &day);
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
        is_leap_year = 1;
    switch (month)
    {
    case 1:
        res = day;
        break;
    case 2:
        res = 31 + day;
        break;
    case 3:
        if (is_leap_year == 1)
            res = 31 + day + 29;
        else
            res = 31 + day + 28;
        break;
    case 4:
        if (is_leap_year == 1)
            res = 31 * 2 + 29 + day;
        else
            res = 31 * 2 + 28 + day;
        break;
    case 5:
        if (is_leap_year == 1)
            res = 31 * 2 + 29 + day + 30;
        else
            res = 31 * 2 + 28 + day + 30;
        break;
    case 6:
        if (is_leap_year == 1)
            res = 31 * 3 + 29 + day + 30;
        else
            res = 31 * 3 + 28 + day + 30;
        break;
    case 7:
        if (is_leap_year == 1)
            res = 31 * 3 + 29 + day + 30 * 2;
        else
            res = 31 * 3 + 28 + day + 30 * 2;
        break;
    case 8:
        if (is_leap_year == 1)
            res = 31 * 4 + 29 + day + 30 * 2;
        else
            res = 31 * 4 + 28 + day + 30 * 2;
        break;
    case 9:
        if (is_leap_year == 1)
            res = 31 * 5 + 29 + day + 30 * 2;
        else
            res = 31 * 5 + 28 + day + 30 * 2;
        break;
    case 10:
        if (is_leap_year == 1)
            res = 31 * 5 + 29 + day + 30 * 3;
        else
            res = 31 * 5 + 28 + day + 30 * 3;
        break;
    case 11:
        if (is_leap_year == 1)
            res = 31 * 6 + 29 + day + 30 * 3;
        else
            res = 31 * 6 + 28 + day + 30 * 3;
        break;
    case 12:
        if (is_leap_year == 1)
            res = 31 * 6 + 29 + day + 30 * 4;
        else
            res = 31 * 6 + 28 + day + 30 * 4;
    }
    printf("%d", res);
    return 0;
}