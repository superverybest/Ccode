#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>

//#define OFFSETOF(type,number) (size_t)&(((type*)0)->number)
//
//struct S 
//{
//
//	char a;
//	int b;
//	int c;
//
//};
//
//int main()
//{
//
//	printf("%d\n", OFFSETOF(struct S, a));
//	printf("%d\n", OFFSETOF(struct S, b));
//	printf("%d\n", OFFSETOF(struct S, c));
//
//	return 0;
//}

#include <stdio.h>

#define SWAP(x) (x=(((x&0x55555555)<<1)+((x&0xaaaaaaaa)>>1)))

int main()
{

	int a = 10;

	printf("%#x\n", a);

	SWAP(a);

	printf("%#x\n", a);

	return 0;

}