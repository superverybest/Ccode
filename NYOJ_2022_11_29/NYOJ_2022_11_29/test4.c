﻿#define _CRT_SECURE_NO_WARNINGS 1
/*
* 描述

Ice Also Heat有一个长度为𝑛的字符串𝑠，只包含字符’a’， ‘b’和’c’。

他想找到最小子串的长度，满足以下条件:

1.子字符串的长度至少为2。
2.‘a’在子字符串中出现的次数严格多于’b’。
3.‘a’在子字符串中出现的次数严格多于’c’。
Ice Also Heat正忙着计划他的下一轮密码部队。快来帮他解决问题。

如果可以通过删除开头的几个(可能是零或全部)字符和结尾的几个(可能是零或全部)字符从𝑏获得𝑎，则字符串𝑎是字符串𝑏的子字符串。

输入描述

第一行包含单个整数𝑡(1≤𝑡≤1e5)—测试用例的数量。测试用例的描述如下。

每个测试用例的第一行包含单个整数𝑛(2≤𝑛≤1e6)—字符串𝑠的长度。

每个测试用例的第二行包含一个字符串𝑠，仅由字符’a’、'b’和’c’组成。

可以保证所有测试用例中𝑛的总和不超过1e6。

输出描述

对于每个测试用例，输出满足给定条件的最小子串的长度，如果没有这样的子串，则输出 - 1。

用例输入 1 

3
2
aa
5
cbabb
8
cacabccc
用例输出 1 

2
-1
3
*/
#define LEN 1000
#include <stdio.h>
int main()
{
	char arr[LEN];
	int times = 0;
	int i = 0;
	int j = 0;
	int len = 0;
	int counta = 0;
	int countb= 0;
	int countc= 0;
	scanf("%d", &times);
	for (i = 0;i < times;i++)
	{
		scanf("%d", &len);

		scanf("%s", arr);
		for (j = 0;j < len;j++)
		{
			if (arr[i] == 'a')
				counta++;
			if (arr[i] == 'b')
				countb++;
			if (arr[i] == 'b')
				countb++;
		}
		if (counta > countc && counta > countb)
		{
			if (counta > countb)
				printf("%d\n", counta);
			else
				printf("%d\n", countb);
		}
		else
		{
			printf("-1\n");
		}
	}
	return 0;
}