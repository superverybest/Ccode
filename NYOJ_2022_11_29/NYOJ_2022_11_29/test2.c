#define _CRT_SECURE_NO_WARNINGS 1
/*
大风车吱呀吱悠悠的转
这里的风景啊真好看
天好看，地好看
还有一群快乐的小伙伴
。。。。。。
不知道你对小时候经常看的大风车栏目是否怀念，让我们一起回忆一下童年的大风车。
现在要求你打印一下风车,比如这样：

输入描述
多组输入输出，每行输入一个n（3<=n<=60），表示要输入一个边长为2*n-1的风车。

输出描述
对于每组数据输出一个边长为2n-1的风车（即2n-1行，每行2*n-1个字符）。每两个风车之间应空上一行

用例输入 1
5
3
用例输出 1
*****   *
 ****  **
  *** ***
   ******
*********
******
*** ***
**  ****
*   *****

*** *
 ****
*****
****
* ***
*/
#include <stdio.h>
#include <string.h>
void initshow(char show[119][119], int n);
int main()
{
    int i = 0;
    int j = 0;
    int n = 0;
    int k = 1;
    int o = 0;
    int q = 0;
    int times = 0;
    char tmp = ' ';
    char show[119][119] = { ' ' };
    scanf("%d", &times);
    for (q = 0;q < times;q++)
    {
        initshow(show, n);
        scanf("%d", &n);
        for (i = 0;i < n;i++)
        {
            show[0][i] = '*';
        }
        for (i = n;i < 2 * n - 2;i++)
        {
            show[0][i] = ' ';
        }
        show[0][2 * n - 2] = '*';
        show[0][2 * n - 1] = 0;
        for (i = 1;i < n;i++)
        {
            strcpy(show[i], show[0]);
        }
        i = 0;
        k = 1;
        j = 2 * n - 3;
        while (i < n - 2)
        {
            tmp = show[k][j];
            show[k][j] = show[k][i];
            show[k][i] = tmp;
            k++;
            i++;
            j--;
            strcpy(show[k], show[k - 1]);
        }
        for (i = 0;i < 2 * n - 1;i++)
        {
            show[k][i] = '*';
        }
        show[k][2 * n - 1] = 0;
        for (i = n, k = 2;i < 2 * n - 1;i++, k++, k += 1)
        {
            for (j = 0, o = 2 * n - 2;j < 2 * n - 1;j++, o--)
            {
                show[i][j] = show[i - k][o];
            }
            show[i][2 * n - 1] = 0;

        }
        for (i = 0;i < 2 * n - 1;i++)
        {
            printf("%s\n", show[i]);
        }
    }
    return 0;
}
void initshow(char show[119][119], int n)
{
    int i = 0;
    int j = 0;
    for (i = 0;i <= 2 * n - 1;i++)
    {
        for (j = 0;j <= 2 * n - 1;j++)
        {
            show[i][j] = ' ';
        }
    }
}