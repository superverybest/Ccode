#define _CRT_SECURE_NO_WARNINGS 1
/*
描述
进acm怎么能少输出图形呢~快！给lk打印个三角形kan kan
输入描述
第一行包含一个整数t（1<=t<=10）。测试用例的数量。测试用例的描述如下。
每个测试用例的第一行包含一个整数n(3<=n<=10)，表示每个三角形的行数（最后一行由2n-1个组成）。
输出描述
对于每一个n，输出一个三角形（只由’*‘和’ '组成）。
用例输入 1
1
3
用例输出 1
  *
 * *
*****
用例输入 2
2
4
5
用例输出 2
   *
  * *
 *   *
*******
	*
   * *
  *   *
 *     *
*********
*/
#define ROW 10
#define COL 10
#include <stdio.h>
void initshow(char arr[ROW][COL], int row, int col,int t);
int main()
{
	char show [ROW][COL] = {' '};
	int i = 0;
	int j = 0;
	int k = 0;
	int t = 0;
	int m = 0;
	int times = 0;
	scanf("%d", &times);
	for (m = 0;m < times;m++)
	{
		scanf("%d", &t);
		initshow(show, t, 2 * t - 1, t);
		i = t - 1;
		j = t - 1;
		for (i = t - 1, j = t - 1, k = 0;k < t;i++, j--, k++)
		{
			show[k][i] = '*';
			show[k][j] = '*';
			if (k == t - 1)
			{
				int x = 0;
				for (x = 0;x < 2 * t - 1;x++)
				{
					show[k][x] = '*';
				}
			}
		}
		for (i = 0;i < t;i++)
		{
			printf("%s\n", show[i]);
		}
	}
	return 0;
}
void initshow(char arr[ROW][COL],int row, int col,int t)
{
	int i = 0;
	int j = 0;
	for (i = 0;i < row;i++)
	{
		for (j = 0;j < col;j++)
		{
			arr[i][j] = ' ';
		}
		arr[i][2*t-1] =0;
	}
}