#define _CRT_SECURE_NO_WARNINGS 1
/*
问题描述：
给你一个正整数， 如果它可以被拆成4个素数之和， 我们就认为是一个 GOOD 数。
例如：
9 = 2 + 2 + 2 + 3
19 = 2 + 2 + 2 + 13
... ...
第一行给你一个正整数 nn 代表测试数据的个数
接下来 nn 行给你一个正整数 aa， 问你它是否是一个 GOOD 数
输入描述
第一行一个正整数 nn (n \leq 10^{3}n≤10
3)
接下来 nn 行， 每行一个正整数 aa (a \leq 10^{18}a≤10
18)
输出描述
输出有 nn 行，对于每一个 aa 如果 a 是 GOOD 数输出 YES 否则输出NO
样例输入：
1
19
样例输出
YES
*/
#include<stdio.h>
int checkPrime(int n);
int main()
{
	int i = 0;
	int j = 0;
	int n = 0;
	int m = 0;
	int x = 0;
	int y = 0;
	int isprime = 0;
	scanf("%d", &n);
	for (i = 0;i < n;i++)
	{
			scanf("%d", &m);
		if (m % 2 == 0)
		{
			if (m / 2 > 2 && m % 2 == 0)
				isprime = 1;
		}
		else if (m % 2 == 1)
		{		
			x = m / 2;
			if (x  > 2 )
			{
				y = x + 1;
				for (j = 2; j <= y / 2; j++)
				{
					if (checkPrime(j) == 1)
					{
						if (checkPrime(y - j) == 1)
						{
							isprime = 1;
						}
					}
				}
			}
			else
			{
				isprime = 0;
			}
		}
		if (isprime == 1)
		{
			printf("YES");
		}
		else
		{
			printf("NO");
		}
		printf("\n");
	}
	return 0;
}
int checkPrime(int n)
{
	int i, isPrime = 1;
	for (i = 2; i <= n / 2; ++i)
	{
		if (n % i == 0)
		{
			isPrime = 0;
			break;
		}
	}
	return isPrime;
}
