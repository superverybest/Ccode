#define _CRT_SECURE_NO_WARNINGS 1
/*
描述
Mostrp是个爱干净的好少年。 有一次去澡堂洗澡时发现 澡堂的澡柜编号中没有出现过数字‘4’
Mostrp 感到很好奇。可能是因为在澡堂老板眼里。数字‘4’是十分不吉利的。
现在Mostrp知道澡柜的最大的编号N，你能帮他算出澡堂一共有多少澡柜吗？

输入描述
有多组数据，每行输入一个N。
( 1 <= N <= 50000 )

输出描述
输出澡柜的个数，输出占一行。

用例输入 1
3
5

用例输出 1
3
4
*/
#include <stdio.h>
int main()
{
	int i = 0;
	int count = 0;
	int n = 0;
	scanf("%d", &n);
	for (i = 1;i <=n;i++)
	{
		if (i == 4 || i / 10 == 4 || i / 100 == 4 || i / 1000 == 4 || i / 10000 == 4)
			;
		else
			count++;
	}
	printf("%d", count);
	return 0;
}