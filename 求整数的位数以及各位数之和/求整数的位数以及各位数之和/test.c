#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <math.h>
int main()
{
	int number = 0, sum = 0;
	int in;
	scanf("%d", &in);
	if (in < 0) in = abs(in);
	while (in > 0)
	{
		sum += in % 10;
		in = in / 10;
		number++;
	}
	printf("number=%d, sum=%d\n", number, sum);
	return 0;
}