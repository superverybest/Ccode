#include "heap.h"

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType x = *p1;
	*p1 = *p2;
	*p2 = x;
}

void AdjustUp(HPDataType *a,HPDataType child)
{
	int parent = (child - 1) / 2;

	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(a[child],a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void HeapInit(HP* php)
{
	assert(php);
	php->a = (HPDataType*)malloc(sizeof(HPDataType)*4);

	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	php->capacity = 4;
	php->size = 0;
}

void HeapPush(HP* php, HPDataType x)
{
	assert(php);

	if (php->capacity == php->size)
	{
		HPDataType* tmp = (HPDataType*)realloc(php->a,sizeof(HPDataType) * php->size * 2);
		if (tmp == NULL)
		{
			perror("malloc fail");
			return;
		}
		php->a = tmp;
		php->capacity*= 2;
	}
	php->a[php->size] = x;
	php->size++;

	AdjustUp(php->a,php->size-1);
}

void AdjustDown(HPDataType* a,int n,int parent)
{
	int child = parent * 2 + 1;

	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
		{
			++child;
		}
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

