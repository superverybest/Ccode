//[root@localhost ~]# chmod + t / home / # 加上粘滞位
//[root@localhost ~]# ls - ld / home /
//drwxrwxrwt. 3 root root 4096 9月 19 16:00 / home /
//[root@localhost ~]# su - litao
//[litao@localhost ~]$ rm / home / abc.c #litao不能删除别人的文件
//rm：是否删除有写保护的普通空文件 "/home/abc.c"？y
//rm : 无法删除"/home/abc.c" : 不允许的操作

//[root@localhost ~]# chmod 0777 / home /
//[root@localhost ~]# ls / home / -ld
//drwxrwxrwx. 3 root root 4096 9月 19 15:58 / home /
//[root@localhost ~]# touch / home / root.c
//[root@localhost ~]# ls - l / home /
//总用量 4
//- rw - r--r--. 1 root root 0 9月 19 15:58 abc.c
//drwxr - xr - x. 27 litao litao 4096 9月 19 15 : 53 litao
//- rw - r--r--. 1 root root 0 9月 19 15 : 59 root.c
//[root@localhost ~]# su - litao
//[litao@localhost ~]$ rm / home / root.c #litao可以删除root创建的文件
//rm：是否删除有写保护的普通空文件 "/home/root.c"？y
//[litao@localhost ~]$ exit
//logout

//范例一：将整个 / etc 目录下的文件全部打包成为 `/tmp / etc.tar`
//[root@linux ~]# tar - cvf / tmp / etc.tar / etc <= = 仅打包，不压缩！
//[root@linux ~]# tar - zcvf / tmp / etc.tar.gz / etc <= = 打包后，以 gzip 压缩
//[root@linux ~]# tar - jcvf / tmp / etc.tar.bz2 / etc <= = 打包后，以 bzip2 压缩
//特别注意，在参数 f 之后的文件档名是自己取的，我们习惯上都用.tar 来作为辨识。
//如果加 z 参数，则以.tar.gz 或.tgz 来代表 gzip 压缩过的 tar file ～
//如果加 j 参数，则以.tar.bz2 来作为附档名啊～
//上述指令在执行的时候，会显示一个警告讯息：
//『`tar: Removing leading `/" from member names`』那是关於绝对路径的特殊设定。
//范例二：查阅上述 / tmp / etc.tar.gz 文件内有哪些文件？
//[root@linux ~]# tar - ztvf / tmp / etc.tar.gz
//由於我们使用 gzip 压缩，所以要查阅该 tar file 内的文件时，就得要加上 z 这个参数了！这很重要的！
//范例三：将 / tmp / etc.tar.gz 文件解压缩在 / usr / local / src 底下
//[root@linux ~]# cd / usr / local / src
//[root@linux src]# tar - zxvf / tmp / etc.tar.gz
//在预设的情况下，我们可以将压缩档在任何地方解开的！以这个范例来说，
//我先将工作目录变换到 / usr / local / src 底下，并且解开 / tmp / etc.tar.gz ，
//则解开的目录会在 / usr / local / src / etc 呢！另外，如果您进入 / usr / local / src / etc
//则会发现，该目录下的文件属性与 / etc / 可能会有所不同喔！
//范例四：在 / tmp 底下，我只想要将 / tmp / etc.tar.gz 内的 etc / passwd 解开而已[root@linux ~]# cd / tmp
//[root@linux tmp]# tar - zxvf / tmp / etc.tar.gz etc / passwd
//我可以透过 tar - ztvf 来查阅 tarfile 内的文件名称，如果单只要一个文件，
//就可以透过这个方式来下达！注意到！ etc.tar.gz 内的根目录 / 是被拿掉了！
//范例五：将 / etc / 内的所有文件备份下来，并且保存其权限！
//[root@linux ~]# tar - zxvpf / tmp / etc.tar.gz / etc
//这个 - p 的属性是很重要的，尤其是当您要保留原本文件的属性时！
//范例六：在 / home 当中，比 2005 / 06 / 01 新的文件才备份
//[root@linux ~]# tar - N "2005/06/01" - zcvf home.tar.gz / home
//比特科技 比特就业课20.bc指令：
//bc命令可以很方便的进行浮点运算
//21.uname –r指令：
//语法：uname[选项]
//功能： uname用来获取电脑和操作系统的相关信息。
//补充说明：uname可显示linux主机所用的操作系统的版本、硬件的名称等基本信息。
//常用选项：
//
//- a或–all 详细输出所有信息，依次为内核名称，主机名，内核版本号，内核版本，硬件名，处理器类
//型，硬件平台类型，操作系统名称
//22.重要的几个热键[Tab], [ctrl] - c, [ctrl] - d
//[Tab]按键-- - 具有『命令补全』和『档案补齐』的功能
//[Ctrl] - c按键-- - 让当前的程序『停掉』
//[Ctrl] - d按键-- - 通常代表着：『键盘输入结束(End Of File, EOF 戒 End OfInput)』的意思；另外，他也可
//以用来取代exit
//23.关机
//语法：shutdown[选项] * *常见选项： * *
//-h ： 将系统的服务停掉后，立即关机。
//- r ： 在将系统的服务停掉之后就重新启动
//- t sec ： - t 后面加秒数，亦即『过几秒后关机』的意思
//以下命令作为扩展 :
//◆ 安装和登录命令：login、shutdown、halt、reboot、install、mount、umount、chsh、exit、last；
//◆ 文件处理命令：file、mkdir、grep、dd、find、mv、ls、diff、cat、ln；
//◆ 系统管理相关命令：df、top、free、quota、at、lp、adduser、groupadd、kill、crontab；
//◆ 网络操作命令：ifconfig、ip、ping、netstat、telnet、ftp、route、rlogin、rcp、finger、mail、 nslookup；
//◆ 系统安全相关命令：passwd、su、umask、chgrp、chmod、chown、chattr、sudo ps、who；
//◆ 其它命令：tar、unzip、gunzip、unarj、mtools、man、unendcode、uudecode。
//范例七：我要备份 / home, / etc ，但不要 / home / dmtsai
//[root@linux ~]# tar --exclude / home / dmtsai - zcvf myfile.tar.gz / home/* /etc
//范例八：将 /etc/ 打包后直接解开在 /tmp 底下，而不产生文件！
//[root@linux ~]# cd /tmp
//[root@linux tmp]# tar -cvf - /etc | tar -xvf -
//这个动作有点像是 cp -r /etc /tmp 啦～依旧是有其有用途的！
//要注意的地方在於输出档变成 - 而输入档也变成 - ，又有一个 | 存在～
//这分别代表 standard output, standard input 与管线命令啦！