#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
int main()
{
	int i = 0;
	float arr[3];
	float sum = 0;
	for (i = 0;i < 3;i++)
	{
		scanf("%f", &arr[i]);
		sum += arr[i];
	}
	float res = sum / 3;
	printf("%f", res);
	return 0;
}