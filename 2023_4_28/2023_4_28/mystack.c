#pragma once
#include "Queue.h"

typedef struct
{
	Queue q1;
	Queue q2;
}MyStack;

MyStack* myStackCreate()
{
	MyStack* pst = (MyStack*)malloc(sizeof(MyStack));
	if (pst == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	QueueInit(&pst->q1);
	QueueInit(&pst->q2);
	return pst;
}

void myStackPush(MyStack* obj,QDadatype x)
{
	if (!QueueEmpty(&obj->q1))
	{
		QueuePush(&obj->q1,x);
	}
	else
	{
		QueuePush(&obj->q2, x);
	}
}

//int myStackPop(MyStack* obj)
//{
//	Queue* emptyQ = &obj->q1;
//	Queue* nonemptyQ = &obj->q2;
//
//	if (!QueueEmpty(emptyQ))
//	{
//		nonemptyQ = &obj->q1;
//		emptyQ = &obj->q2;
//	}
//
//	while (QueueSize(&obj->q1)>1)
//	{
//		QueuePush(emptyQ, QueueFront(nonemptyQ));
//		QueuePop(nonemptyQ);
//	}
//
//	int top = QueueFront(nonemptyQ);
//	return top;
//}


int myStackPop(MyStack* obj)
{
	Queue* emptyQ = &obj->q1;
	Queue* nonemptyQ = &obj->q2;

	if (!QueueEmpty(&obj->q1))
	{
		emptyQ= &obj->q2;
		nonemptyQ= &obj->q1;
	}

	while (QueueSize(nonemptyQ) > 1)
	{
		QueuePush(emptyQ, QueueFront(nonemptyQ));
		QueuePop(nonemptyQ);
	}

	int top = QueueFront(nonemptyQ);
	QueuePop(nonemptyQ);
	return top;
}

int myStackTop(MyStack* obj)
{
	if (!QueueEmpty(&obj->q1))
	{
		return QueueTail(&obj->q1);
	}
	else
	{
		return QueueTail(&obj->q2);
	}
}

bool myStackEmpty(MyStack* obj)
{
	return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

void myStackFree(MyStack* obj)
{
	QueueDestroy(&obj->q1);
	QueueDestroy(&obj->q2);
	free(obj);
}
