#pragma once

#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
typedef int QDadatype;

typedef struct QNode
{
	struct QNode* next;
	QDadatype data;
}QNode;

typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Queue;

void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);
void QueuePush(Queue* pq, QDadatype x);
void QueuePop(Queue* pq);
int QueueSize(Queue* pq);
bool QueueEmpty(Queue* pq);
QDadatype QueueFront(Queue* pq);
QDadatype QueueTail(Queue* pq);