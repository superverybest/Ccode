#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
int main()
{
	int x = 0;
	printf("请输入要拆分的偶数\n");
	scanf("%d",&x);
	if ((x & (-x)) == 1)
	{
		printf("%d是奇数", x);
	}
	else if ((x & (-x)) == 0)
	{
		printf("%d是零", x);
	}
	else 
	{
		int even_number = x & (-x);
		int odd_number = x / even_number;
		printf("%d = %d × %d", x, even_number, odd_number);
	}
	return 0;
}