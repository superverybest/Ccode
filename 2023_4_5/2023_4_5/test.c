#define _CRT_SECURE_NO_WARNINGS 1

#include "list.h"

void test()
{

	LTNode* list=LTInit();

	LTPushBack(list,5);
	LTPrint(list);
	LTPushBack(list, 6);
	LTPrint(list);
	LTPushBack(list, 7);
	LTPrint(list);
	LTPopBack(list);
	LTPrint(list);
	LTPushFront(list, 0);
	LTPrint(list);
	LTPushFront(list, 0);
	LTPrint(list);
	LTInsert(list->next->next->next, 100);
	LTPrint(list);
	LTErase(list->next->next->next);
	LTPrint(list);

}

int main()
{

	test();

	return 0;
}