#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int diff(int n, int m)
{
    int count = 0;
    int i = 0;
    for (i = 0;i < 32;i++)
    {
        if (((n >> i) & 1) != ((m >> i) & 1))
            count++;
    }
    return count;
}
int main()
{
    int n = 0;
    int m = 0;
    int ret = 0;
    scanf("%d%d", &n, &m);
    ret = diff(n, m);
    printf("%d", ret);
    return 0;
}