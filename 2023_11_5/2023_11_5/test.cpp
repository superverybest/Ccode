#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include <iostream>
#include <functional>

//C++98设为私有
class CopyBan
{
public:
	CopyBan(int i)
		:_i(i)
	{}

private:
	CopyBan(const CopyBan&);
	CopyBan& operator=(const CopyBan&);
	int _i;

};

//C++11 使用delete，表示删去默认成员函数
class CopyBan2
{
public:
	CopyBan2(int i)
		:_i(i)
	{}

private:
	CopyBan2(const CopyBan2&) = delete;
	CopyBan2& operator=(const CopyBan2&) = delete;
	int _i;

};

//C++98
class HeapOnly
{
public:
	static HeapOnly* CreateThis()
	{
		return new HeapOnly;
	}

private:
	HeapOnly(){}
	HeapOnly(const HeapOnly&);

};

//C++11
class HeapOnly2
{
public:
	static HeapOnly2* CreateThis()
	{
		return new HeapOnly2;
	}

private:
	HeapOnly2() {}
	HeapOnly2(const HeapOnly2&) = delete;

};


int main()
{
	CopyBan test0(100);
	CopyBan test1(100);

	//test0(test1);
	//test0=test1;

	CopyBan2 test2(100);
	CopyBan2 test3(100);

	//test2(test3);
	//test2 = test3;

	//HeapOnly test4=new HeapOnly;
	HeapOnly* test5 = HeapOnly::CreateThis();
	//HeapOnly2 test4=new HeapOnly2;
	HeapOnly2* test6 = HeapOnly2::CreateThis();

	return 0;
}