#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int count[26] = { 0 };
	int maxindex = 0;
	char ch;
	while ((ch = getchar()) != '\n')
	{
		if (++count[ch - 97] >= count[maxindex])
			maxindex = ch - 97;



	}

	printf("%c\n%d", maxindex + 97, count[maxindex]);


	return 0;
}


//#include<bits/stdc++.h>
//using namespace std;
//int N;
//const int SIZE = 1e4 + 4;
//char area[SIZE][SIZE];
//bool flag;
//int cnt;
//int d[4][2] = {
//    {1,0},
//    {-1,0},
//    {0,1},
//    {0,-1}
//};
////注意：求的是被淹没的岛屿的数量   总岛屿数量-被淹没的岛屿的数量 
//int ans = 0;//没有被淹没岛屿的数量 
//int res_ans = 0;//岛屿的总数量 
////用DFS判断搜到的这个岛屿会不会被淹没，仅此而已，不需要返回什么 昨判断关系
//void dfs(int x, int y)
//{
//    if (flag == false) { //一个岛屿只要有一个点满足就不会变淹没了
//        cnt = 0;
//        for (int i = 0; i < 4; i++) {
//            int tx = d[i][0] + x;
//            int ty = d[i][1] + y;
//            if (area[tx][ty] != '.')
//                cnt++;
//        }
//        if (cnt == 4) {//有一个点满足不会被淹没的条件
//            ans++;
//            flag = true;//这个岛屿不需要再遍历了
//        }
//    }
//    area[x][y] = '*';//将遍历过的点变为 *，下一次就不会遍历他了，所以不用标记数组
//    //注意这里不可以是‘.’因为上面if(area[tx][ty]!='.')cnt++
//    for (int i = 0;i < 4;i++) {
//        int xx = x + d[i][0];
//        int yy = y + d[i][1];
//        if (area[xx][yy] == '#' && x < N && x >= 0 && y < N && y >= 0)
//            dfs(xx, yy);
//    }
//}
//
//int main()
//{
//    cin >> N;
//    for (int i = 0; i < N; i++)
//        for (int j = 0; j < N; j++)
//            cin >> area[i][j];
//
//    for (int i = 0; i < N; i++) {
//        for (int j = 0; j < N; j++) {
//            if (area[i][j] == '#') {
//                res_ans++;
//                flag = false;
//                dfs(i, j);
//            }
//        }
//    }
//    cout << res_ans - ans;
//    return 0;
//}