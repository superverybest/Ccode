#define _CRT_SECURE_NO_WARNINGS 1



//void* my_memcpy(void* dst, const void* src, size_t count)
//{
//	void* ret = dst;
//	assert(dst);
//	assert(src);
//	while (count--)
//	{
//		*(char*)dst = *(char*)src;
//		dst = (char*)dst + 1;
//		src = (char*)src + 1;
//	}
//	return(ret);
//}
//int main()
//{
//	char arr1[20] = "MAGA";
//	char arr2[20] = "donald trump";
//
//	char* ret =my_memcpy(arr1,arr2,6);
//
//	printf("%s", ret);
//	return 0;
//}

#include <stdio.h>
#include <assert.h>
void* my_memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}

	return ret;
}
int main()
{
	char arr1[100] = "maka american great     again destroyed";
	void* arr = my_memmove(arr1+14,arr1+31,9);
	printf("%s\n", arr);
	printf("%s\n", arr1);
	return 0;
}
