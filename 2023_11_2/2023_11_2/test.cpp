#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include <iostream>

template<class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr)
		:_ptr(ptr)
	{}

	~SmartPtr()
	{
		cout << "delete:" << _ptr << endl;
		delete _ptr;
	}

	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}

private:
	T* _ptr;

};

int main()
{
	SmartPtr<string> sp1(new string("xxx"));
	SmartPtr<string> sp2(new string("yyy"));

	return 0;
}