#define _CRT_SECURE_NO_WARNINGS 1
//#include <iostream>
//using namespace std;
//
//
//int& fib(int n, int& f, int& f1, int& f2, int i)
//{
//    if (n <= 2)
//    {
//        int res = 1;
//        int& ress = res;
//        return ress;
//    }
//    f = f1 + f2;
//    f1 = f2;
//    f2 = f;
//    if (n > 2 && i < n)
//    {
//        fib(n, f, f1, f2, i + 1);
//    }
//    else if (i >= n)
//    {
//        return f;
//    }
//    return f;
//}
//
//int main()
//{
//    int n = 0;
//    cin >> n;
//    int f1 = 1;
//    int f2 = 1;
//    int f = 0;
//    int i = 3;
//    cout << fib(n, f, f1, f2, i);
//    return 0;
//}

#include <iostream>
#include <string>
using namespace std;

bool pattern(const char* str1, const char* str2)
{
    if (*str1 == '\0' && *str2 == '\0')
    {
        return true;
    }
    if (*str1 == '\0' || *str2 == '\0')
    {
        return false;
    }

    if (*str1 == '?')
    {
        return pattern(str1 + 1, str2 + 1);
    }
    else if (*str1 == '*')
    {
        return pattern(str1 + 1, str2 + 1) || pattern(str1, str2 + 1) || pattern(str1 + 1, str2);
    }
    else if (*str1 == *str2)
    {
        return pattern(str1 + 1, str2 + 1);
    }
    else return false;



    return false;
}

int main()
{
    string first;
    string second;
    getline(cin, first);
    getline(cin, second);
    if (pattern(first.c_str(), second.c_str()))
    {
        cout << "true";
    }
    else
    {
        cout << "false";
    }
    return 0;
}