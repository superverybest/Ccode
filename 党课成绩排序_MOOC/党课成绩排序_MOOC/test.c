#define _CRT_SECURE_NO_WARNINGS 1
/*
用结构类型表示学生党员信息(包括学号、姓名和党课成绩)。输入整数n(n<10)，再输入n个学生党员的基本信息存放在结构数组中，要求按学生的党课成绩升序排列。

输出格式为printf("%s %s %.1f\n",?)

输入样例：

3

20180501 zhangsan 90.5

20180502 wangfang 88.5

20180503 liqiang 92.5

输出样例：

20180502 wangfang 88.5

20180501 zhangsan 90.5

20180503 liqiang 92.5

*/
#include <stdio.h>
int main()
{
	struct students { char id[10];char name[10];float score; }inform[10], tmp;
	int n = 0;
	int i = 0;
	int j = 0;
	scanf("%d", &n);
	for (i = 0;i < n;i++)
	{
		scanf("%s%s%f", &inform[i].id, &inform[i].name, &inform[i].score);
	}
	for (i = 0;i < n - 1;i++)
	{
		for (j = i + 1;j < n;j++)
		{
			if (inform[i].score > inform[j].score)
			{
				tmp = inform[j];
				inform[j] = inform[i];
				inform[i] = tmp;
			}
		}
	}
	for (i = 0;i < n;i++)
	{
		printf("%s %s %.1f\n", inform[i].id, inform[i].name, inform[i].score);
	}
}