#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include <iostream>

//class StackOnly
//{
//public:
//	static StackOnly CreatThis()
//	{
//		return StackOnly();
//	}
//
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//
//private:
//	StackOnly()
//		:_a(0)
//	{}
//
//	int _a;
//
//};

//class StackOnly
//{
//public:
//	static StackOnly CreatThis()
//	{
//		return StackOnly();
//	}
//
//private:
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//
//
//	StackOnly()
//		:_a(0)
//	{}
//
//		int _a;
//
//};

class StackOnly
{
public:
	static StackOnly CreatThis()
	{
		return StackOnly();
	}


private:
	void* operator new(size_t size) = delete;
	void operator delete(void* p) = delete;

	StackOnly()
		:_a(0)
	{}

	int _a;

};

class NonInHerit
{
public:
	static NonInHerit GetInstance()
	{
		return NonInHerit();
	}

private:
	NonInHerit()
	{}

};

//class TestClass :NonInHerit
//{
//	TestClass()
//	{
//
//	}
//
//};

int main()
{
	StackOnly test0 = StackOnly::CreatThis();
	NonInHerit test1 = NonInHerit::GetInstance();
	//TestClass test2;

	return 0;
}