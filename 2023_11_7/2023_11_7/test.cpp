#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include <iostream>

//int main()
//{
//	const int n = 10;
//
//	int* p = (int*)&n;
//	(*p)++;
//
//	cout << n << endl;
//	cout << (*p) << endl;
//
//	return 0;
//}

//int main()
//{
//	volatile const int n = 10;
//
//	int* p = (int*)&n;
//	(*p)++;
//
//	cout << n << endl;
//	cout << (*p) << endl;
//
//	return 0;
//}

class TestClass0
{

};

class TestClass1 :public TestClass0
{

};

int main()
{
	TestClass0 test0;
	TestClass1 test1;

	TestClass0* p0 = new TestClass1;
	TestClass1* p1 = (TestClass1*)p0;

	char* c0 = (char*)new int(5);
	//char* c1 = static_cast<char*>(new int(5));

	return 0;
}