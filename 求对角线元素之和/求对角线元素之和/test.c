#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main(void)
{
	int i = 0, j = 0, n = 0, sum = 0;
	int a[6][6];
	scanf("%d", &n);
	for (i = 0;i < n;i++)
	{
		for (j = 0;j < n;j++)
		{
			scanf("%d", &a[i][j]);
		}
	}
	if (n % 2 == 1)
	{
		for (i = 0;i < n;i++)
		{
			for (j = 0;j < n;j++)
			{
				if (i + j == n - 1 || i == j)
				{
					sum += a[i][j];
				}
			}
		}
	}
	if (n % 2 == 0)
	{
		for (i = 0;i < n;i++)
		{
			for (j = 0;j < n;j++)
			{
				if (i + j == n - 1 || i == j)
				{
					sum += a[i][j];
				}
			}
		}
	}
	printf("sum is %d\n", sum);
	return 0;
}