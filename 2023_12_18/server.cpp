#include <httplib.h>

void Hello(const Request &req,Response &rsp)

{
  rsp.set_content("Hello World!","text/plain");
  rsp.status=200;
  
}

void Numbers()

int main()
{
  httplib::Server server;
  server.Get("/hi",Hello);
  server.Get(R("/numbers/(\d+)"),Numbers);
  
  return 0;
}
