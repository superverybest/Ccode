#include "Stack.h"

typedef struct
{
	ST pushst;
	ST popst;
}MyQueue;

MyQueue* myQueueCreate()
{
	MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
	if (obj == NULL)
	{
		perror("malloc fail");
		return;
	}

	STInit(&obj->popst);
	STInit(&obj->pushst);

	return obj;
}

void myQueuePush(MyQueue* obj,int x)
{
	STPush(&obj->pushst,x);
}

int myQueuePeek(MyQueue* obj)
{
	if (Empty(&obj->popst))
	{
		while (!STEmpty(&obj->pushst))
		{
			STPush(&obj->popst, &obj->pushst);
			STPop(&obj->pushst);
		}
	}

	return STTop(&obj->popst);
}

int myQueuePop(MyQueue* obj)
{
	int front = myQueuePeek(obj);
	STPop(&obj->pushst);
	return front;
}

bool myQueueEmpty(MyQueue* obj)
{
	return STEmpty(&obj->popst) && STEmpty(&obj->pushst);
}

void myQueueFree(MyQueue* obj)
{
	STDestroy(&obj->popst);
	STDestroy(&obj->pushst);
	free(obj);
}