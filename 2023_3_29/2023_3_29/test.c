#define _CRT_SECURE_NO_WARNINGS 1

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* removeElements(struct ListNode* head, int val)
{
    struct ListNode* cur = head;
    if (head == NULL)
        return head;
    while (cur->next != NULL)
    {
        if (head->val == val)
        {
            head = head->next;
            cur = head;
        }
        else if (cur->next->val == val)
        {
            cur->next = cur->next->next;
        }
        else
        {
            cur = cur->next;
        }
    }
    if (cur->val == val)
        return NULL;
    return head;
}