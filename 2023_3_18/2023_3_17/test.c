#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <assert.h>
//#include <stdlib.h>
//
//#define INIT_CAPACITY 4
//typedef int SLDataType;
//
//typedef struct SeqList
//{
//	SLDataType* a;
//	int size;
//	int capacity;
//}SL;
//
//void SLInit(SL* ps)
//{
//	ps->a = (SL*)malloc(sizeof(SL)* INIT_CAPACITY);
//	if (ps->a == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//	ps->size = 0;
//	ps->capacity = INIT_CAPACITY;
//}
//
//void SLCheckCapacity(SL* ps)
//{
//	assert(ps);
//	if (ps->size == ps->capacity)
//	{
//		SLDataType* tmp = (SL*)realloc(ps->a,sizeof(SLDataType)*ps->capacity*2);
//		if (tmp == NULL)
//		{
//			perror("realloc fail");
//			return;
//		 }
//		ps->a = tmp;
//		ps->capacity *= 2;
//	}
//}
//
//void SLInsert(SL*ps,int pos, SLDataType x)
//{
//	assert(ps);
//	assert(pos >= 0 && pos <= ps->size);
//	SLCheckCapacity(ps);
//	int end = ps->size- 1;
//	while (end>=pos)
//	{
//		ps->a[end + 1] = ps->a[end];
//		--end;
//	}
//	ps->a[pos] = x;
//	ps->size++;
//}
//
//int main()
//{
//
//
//	return 0;
//}

#include<assert.h>
#include<stdio.h>
#include<stdlib.h>

typedef int SLDataType;
#define INIT_CAPACITY 4

typedef struct SeqList
{
	SLDataType* a;
	int size;
	int capacity;
}SL;

void SLInit(SL* ps)
{
	assert(ps);

	ps->a = (SLDataType*)malloc(sizeof(SLDataType) * INIT_CAPACITY);
	if (ps->a == NULL)
	return;

	ps->size = 0;
	ps->capacity = INIT_CAPACITY;
}

void CheckCapacity(SL* ps)
{
	assert(ps);

	if (ps->size == ps->capacity)
	{
		SLDataType* tmp = (SLDataType*)realloc(ps->a, sizeof(SLDataType) * ps->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->a = tmp;
		ps->capacity *= 2;
	}
}

void SLInsert(SL*ps,int pos,SLDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		--end;
	}
	ps->a[pos] = x;
	ps->size++;
}

void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(pos>= 0 && pos <= ps->size);
	int begin = pos + 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;
}

void PushFront(SL* ps, SLDataType x)
{
	SLInsert(ps, 0, x);
}

void PushBack(SL* ps, SLDataType x)
{
	SLInsert(ps, ps->size, x);
}

void PopFront(SL* ps)
{
	SLErase(ps, 0);
}

void PopBack(SL* ps)
{
	SLErase(ps, ps->size-1);
}

int SLFind(SL* ps, SLDataType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}

	return -1;
}

void SLPrint(SL* ps)
{
	assert(ps);

	for (int i = 0; i < ps->size; ++i)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

int main()
{
	SL test;
	SLInit(&test);
	PushBack(&test, 1);
	PushBack(&test, 2);
	PushBack(&test, 3);
	PushBack(&test, 4);

	SLPrint(&test);

	PopFront(&test);

	SLPrint(&test);

	PopBack(&test);

	SLPrint(&test);

	PushFront(&test, 100);

	SLPrint(&test);

	PushBack(&test, 90);

	SLPrint(&test);

	int ret = SLFind(&test, 2);
	printf("找到了，下标是%d", ret);

	return 0;
}