#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include <iostream>

//class MyString {
//public:
//	/*explicit */MyString(int x) {};
//};
//
//int main()
//{
//	MyString test0(10);
//	test0 = 10;
//
//	return 0;
//}

class MyString {
public:
	MyString(int x) {};

	operator int()
	{
		return 0;
	}

	//explicit operator int()
	//{
	//	return 0;
	//}

};

int main()
{
	MyString test0(10);
	test0 = 10;

	MyString test1(10);
	test1 = 1;
	int x = 0;
	x = test1;

	return 0;
}