#include <iostream>

template<typename T,typename Pred>
int countMatchElements(T* beg, T* end, Pred pred)
{
	int result = 0;
	for (;beg != end;++beg)
	{
		if (pred(*beg))
			++result;
	}
	return result;
}

template<typename T>
struct Greater
{
	T mVal;
	explicit Greater(T value) :mVal(value) {};
	bool operator() (const T& val )const {return val > mVal;}
};

int main()
{
	int intarray[] = { 11,16,21,19,17,30 };
	Greater<int> greater20(20);
	std::cout << countMatchElements(intarray, intarray + 6, greater20) << std::endl;
	return 0;
}