#define _CRT_SECURE_NO_WAENINGS 1

#include <stdio.h>

#define INIT_CAPACITY 4

typedef struct SL
{
	int* value;

	int capacity;
	int size;

}SL;

typedef int SLDataType;
void SLInit(SL* ps)
{
	ps->value = (SLDataType*)malloc(sizeof(SLDataType) * INIT_CAPACITY);
	if (ps->value == NULL)
	{
		perror("malloc fail");
		return;
	}

	ps->size = 0;
	ps->capacity = INIT_CAPACITY;
}

void insert(SL* ps,int x)
{
	int i = 0;
	int j = 0;
	while (i <= ps->size && x <= ps->value[i])
		++i;
	for (j = ps->size-1;j >=i;--j)
	{
		ps->value[j + 1] = ps->value[j];
	}
	ps->value[i] = x;
	ps->size++;
}

void insert_union(SL* ps1, SL* ps2, SL* SL_union)
{
	int i = 0;
	int j = 0;
	int k = 0;
	while (i < ps1->size && j < ps2->size)
	{
		if (ps1->value[i] < ps2->value[j])
		{
			SL_union->value[k] = ps1->value[i];
			++i;++k;
		}
		else if (ps1->value[i] >= ps2->value[j])
		{
			SL_union->value[k] = ps2->value[j];
			++j;++k;
		}
	}
	while (i < ps1->size)
	{
		SL_union->value[k] = ps1->size[i];
		++i;++k;
	}
	while (j < ps2->size)
	{
		SL_union->value[k] = ps1->size[j];
		++j;++k;
	}
	SL_union->size = k;
}

int main()
{


	return 0;
}