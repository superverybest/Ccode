//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int w = 0;
//    int h = 0;
//    cin >> w >> h;
//    vector<int> a;
//    a.resize(w,1);
//    vector<vector<int>> b;
//    b.resize(h,a);
//    int count = 0;
//    //for (auto e : b)
//    //{
//    //    for (int i = 0;i < a.size();i++)
//    //    {
//    //        e[i] = 1;
//    //    }
//    //}
//
//    //for (int i = 0;i < w;i++)
//    //{
//    //    for (int j = 0;j < h;j++)
//    //    {
//    //        b[i][j] = 1;
//    //    }
//    //}
//    for (int i = 0;i < h;i++)
//    {
//        for (int j = 0;j < w;j++)
//        {
//            if (b[i][j] == 1)
//                count++;
//
//            if (i + 2 < h)
//                b[i + 2][j] = 0;
//
//            if (j + 2 < w)
//                b[i][j + 2] = 0;
//        }
//    }
//    cout<< count;
//    return 0;
//}

//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int w = 0;
//    int h = 0;
//    cin >> w >> h;
//    vector<int> a;
//    a.resize(w, 1);
//    vector<vector<int>> b;
//    b.resize(h, a);
//    int count = 0;
//    for (int i = 0;i < h;i++)
//    {
//        for (int j = 0;j < w;j++)
//        {
//            if (b[i][j] == 1)
//            {
//                count++;
//                if (i + 2 < h)
//                    b[i + 2][j] = 0;
//                else if (j + 2 < w)
//                    b[i][j + 2] = 0;
//            }
//        }
//    }
//    cout << count;
//    return 0;
//}


class Solution {
public:
    int StrToInt(string str)
    {
        int x = 1;
        int res = 0;
        if (str[0] == '-')
            x = -1;
        if (str[0] == '-' || str[0] == '+')
            str[0] = '0';
        for (auto e : str)
        {
            if (e < '0' || e>'9')
            {
                return 0;
            }
            else
            {
                res = res * 10 + (e - '0');
            }
        }
        return res * x;
    }
};