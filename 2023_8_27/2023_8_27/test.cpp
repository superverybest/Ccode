#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include<iostream>

class Solution {
public:
	string tree2str(TreeNode* root)
	{
		if (root == nullptr)
			return "";

		string str = to_string(root->val);

		str += '(';
		str += tree2str(root->left);
		str += ')';

		str += '(';
		str += tree2str(root->right);
		str += ')';
	}


};