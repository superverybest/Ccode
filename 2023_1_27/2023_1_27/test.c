#define _CRT_SECURE_NO_WARNINGS 1



//#include <stdio.h>
//int main()
//{
//	struct S1
//	{
//		char c1;
//		int i;
//		char c2;
//	};
//	printf("%d\n", sizeof(struct S1));
//	struct S2
//	{
//		char c1;
//		char c2;
//		int i;
//	};
//	printf("%d\n", sizeof(struct S2));
//	return 0;
//}


//#include <stdio.h>
//#include <stddef.h>
//int main()
//{
//	struct S1
//	{
//		char c1;
//		int i;
//		char c2;
//	};
//	printf("%d\n", offsetof(struct S1,c1));
//	printf("%d\n", offsetof(struct S1, i));
//	printf("%d\n", offsetof(struct S1, c2));
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	struct S3
//	{
//		double d;
//		char c;
//		int i;
//	};
//	struct S4
//	{
//		char c1;
//		struct S3 s3;
//		double d;
//	};
//	return 0;
//}



#include <stdio.h>
int main()
{
	struct S1
	{
		char c1;
		int i;
		char c2;
	};
	struct S2
	{
		char c1;
		char c2;
		int i;
	};
	printf("%d\n", sizeof(struct S1));
	printf("%d\n", sizeof(struct S2));
	return 0;
}