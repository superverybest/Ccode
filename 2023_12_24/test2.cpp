#include <iostream>

using namespace std;



#define MaxSize 100  

typedef struct {

    int data[MaxSize];

    int length;

} Sqlist;



void ListInsert(Sqlist *&L, int i, int e) {

    if (i < 1 || i > L->length + 1) {

        return;

    }

    if (L->length >= MaxSize) {

        return;

    }



    for (int j = L->length; j >= i; j--) {

        L->data[j + 1] = L->data[j];

    }

    L->data[i] = e;

    L->length++;

}



void dispList(Sqlist *&L) {

    for (int i = 0; i < L->length; i++) {

        cout << L->data[i] << " ";

    }

    cout << endl;

}



int main() {

    Sqlist *L = new Sqlist();

    L->length = 4;

    L->data[0] = 10;

    L->data[1] = 20;

    L->data[2] = 30;

    L->data[3] = 40;



    dispList(L);

    return 0;
}
