using namespace std;

#include <iostream>
#include <assert.h>
#include <stdio.h>

typedef struct Seqlist
{
  int* array;
  size_t size;
  size_t capacity;
}SL;

void SLCheckCapacity(SL* ps)
{
  assert(ps);

  if(ps->size==ps->capacity)
  {
    int* tmp=(int*)realloc(ps->array,sizeof(int)*ps->capacity*2);
    if(tmp==nullptr)
    {
      perror("realloc fail");
      return;
    }
    ps->array=tmp;
    ps->capacity*=2;
  } 
}

void SLInsert(SL* ps,int pos,int x)
{
  assert(ps);
  assert(pos>=0&&pos<=ps->size);
  
  SLCheckCapacity(ps);

  int end=ps->size-1;
  while(end>=pos)
  {
    ps->array[end+1]=ps->array[end];
    --end;
  }
  ps->array[pos]=x;
  ps->size++;

}

void SLprint(SL* ps)
{
  assert(ps);

  for(int i=0;i<ps->size;++i)
  {
    printf("%d",ps->array[i]);
    printf("\n");
  }

}

void SLinit(SL* ps)
{
  assert(ps);

  ps->array=(int*)malloc(sizeof(int)*4);
  if(ps->array==nullptr)
  {
    perror("malloc fail");
    return;
  }
  ps->size=0;
  ps->capacity=4;
}

int main()
{
  SL sl;
  SLinit(&sl);
  SLInsert(&sl,0,10);
  SLInsert(&sl,1,20);
  SLInsert(&sl,2,30);
  SLInsert(&sl,3,40);
  SLprint(&sl);
  return 0;
}
