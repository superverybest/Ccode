#include "SList.h"

void SLTPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur)
	{
		printf("%d->", cur->Data);
		cur = cur->Next;
	}
	printf("NULL\n");
}

SLTNode* BuyListNode(SLTDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	newnode->Data = x;
	newnode->Next = NULL;
	return newnode;
}

//void SLTPushBack(SLTNode** pphead, SLTDataType x)
//{
//	SLTNode* newnode = BuyListNode(x);
//	if (*pphead == NULL)
//	{
//		*pphead = newnode;
//	}
//	else
//	{
//		SLTNode* tail = *pphead;
//		while (tail->Next != NULL)
//		{
//			tail = tail->Next;
//		}
//		tail->Next = newnode;
//	}
//}

//void SLTPushNode(SLTNode** pphead, SLTDataType x)
//{
//	SLTNode* newnode = BuyListNode(x);
//
//	if (*pphead == NULL)
//	{
//		pphead = newnode;
//	}
//	else
//	{
//		SLTNode* tail = *pphead;
//		while (tail->Next != NULL)
//		{
//			tail = tail->Next;
//		}
//		tail->Next = newnode;
//	}
//}


void SLTPushBack(SLTNode** pphead,SLTDataType x)
{
	SLTNode* newnode = BuyListNode(x);
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->Next != NULL)
		{
			tail = tail->Next;
		}
		tail->Next = newnode;
	}
}

void SLTPushFront(SLTNode** pphead,SLTDataType x)
{
	SLTNode* newnode = BuyListNode(x);
	newnode->Next=*pphead;
	*pphead = newnode;
}

//void SLTPopBack(SLTNode** pphead)
//{
//	assert(*pphead);
//	if ((*pphead)->Next == NULL)
//	{
//		free(*pphead);
//		*pphead = NULL;
//	}
//	else
//	{
//
//		SLTNode* tail = *pphead;
//		while (tail->Next->Next != NULL)
//		{
//			tail = tail->Next;
//		}
//
//		free(tail->Next);
//		tail->Next = NULL;
//	}
//}


void SLTPopBack(SLTNode** pphead)
{
	if ((*pphead)->Next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* tail = *pphead;
		while (tail->Next->Next != NULL)
		{
			tail = tail->Next;
		}
		free(tail->Next);
		tail->Next = NULL;
	}
}

void SLTPopFront(SLTNode** pphead)
{
	SLTNode* first = *pphead;
	*pphead = first->Next;
	free(first);
	first = NULL;
}

SLTNode* SLTFind(SLTNode** pphead, SLTDataType x)
{
	SLTNode* cur = *pphead;
	while (cur)
	{
		if (cur->Data == x)
		{
			return cur;
		}
		cur = cur->Next;
	}
	return NULL;
}

//void SLTErase(SLTNode** pphead, SLTNode* pos)
//{
//	assert(pphead);
//	assert(pos);
//	if (*pphead == pos)
//	{
//		SLTPopFront(*pphead);
//	}
//	else
//	{
//		SLTNode* prev = *pphead;
//		while (prev->Next != pos)
//		{
//			prev = prev->Next;
//		}
//		prev->Next = pos->Next;
//		free(pos);
//		pos = NULL;
//	}
//}

void SLTErase(SLTNode** pphead, SLTNode* pos)
{
	assert(pphead);
	assert(pos);

	if (*pphead == pos)
	{
		SLTPopFront(pphead);
	}
	else
	{
		SLTNode* prev = *pphead;
		while (prev->Next != pos)
		{
			prev = prev->Next;
		}
		prev->Next = pos->Next;
		free(pos);
		pos = NULL;
	}
}

void SLTInsertAfter(SLTNode* pos, SLTDataType x)
{
	assert(pos);
	SLTNode* newnode = BuyListNode(x);
	newnode->Next = pos->Next;
	pos->Next = newnode;
}

void SLTEraseAfter(SLTNode* pos)
{
	assert(pos);
	assert(pos->Next);
	SLTNode* del = pos->Next;
	pos->Next = del->Next;
	free(del);
	del = NULL;
}
