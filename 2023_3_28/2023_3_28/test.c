#define _CRT_SECURE_NO_WARNINGS 1
#include "SList.h"

int main()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPrint(plist);
	SLTInsertAfter(plist, 88);
	SLTPrint(plist);
	SLTPushFront(&plist, 0);
	SLTPrint(plist);
	SLTPushBack(&plist, 100);
	SLTPrint(plist);
	return 0;
}