#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int mystrlen(int num, int* a)
{
	int n = num - 1;
	int i = 0;
	int tmp = 0;
	for (i = 0, n = num - 1;i < n;i++, n--)
	{
		tmp = *(a + i);
		*(a + i) = *(a + n);
		*(a + n) = tmp;
	}
}
int main()
{
	int num = 0;
	int a[10] = { 0 };
	int* p;
	int i = 0;
	scanf("%d", &num);
	for (i = 0;i < num;i++)
	{
		scanf("%d", &a[i]);
	}
	p = &a[0];
	mystrlen(num, p);
	for (i = 0;i < num;i++)
	{
		printf("%d ", a[i]);
	}
	return 0;
}