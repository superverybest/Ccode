#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    while (cin >> n)
//    {
//        if (n <= 2)
//        {
//            cout << -1 << endl;
//        }
//        else if (n % 4 == 1 || n % 4 == 3)
//        {
//            cout << 2 << endl;
//        }
//        else if (n % 4 == 2)
//        {
//            cout << 4 << endl;
//        }
//        else
//        {
//            cout << 3 << endl;
//        }
//    }
//    return 0;
//}

#include <stdio.h>
#include <string.h>

int main()
{
	char buf[1024] = { 0 };
	gets(buf);
	char ch;
	scanf("%c", &ch);
	int len = strlen(buf);
	int count[128] = { 0 };
	for (int i = 0;i < len;i++)
	{
		count[buf[i]] += 1;
	}
	int res = count[ch];
	if (ch >= 'a' && ch <= 'z')
	{
		res += count[ch - 32];
	}
	else if (ch >= 'A' && ch <= 'Z')
	{
		res += count[ch + 32];
	}
	printf("%d\n", res);
	return 0;
}