#define _CRT_SECURE_NO_WARNINGS 1

//% 定义初始值
//S0 = 980;% 初始易感人数
//I0 = 20;% 初始感染人数
//R0 = 0;% 初始康复人数
//N = S0 + I0 + R0;% 总人数
//beta = 0.3;% 接触率
//gamma = 0.1;% 恢复率
//tspan = [0 110];% 时间跨度
//
//% 定义ODE方程
//f = @(t, y)[-beta * y(1) * y(2) / N; beta* y(1)* y(2) / N - gamma * y(2); gamma* y(2)];
//
//% 解ODE方程
//[t, y] = ode45(f, tspan, [S0; I0; R0]);
//
//% 绘制曲线
//plot(t, y(:, 1), 'r', t, y(:, 2), 'g', t, y(:, 3), 'b');
//legend('易感人数', '感染人数', '康复人数');
//xlabel('时间');
//ylabel('人数');