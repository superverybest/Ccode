#include <vector>
class Gift {
public:
    int getValue(vector<int> gifts, int n) {
        sort(gifts.begin(), gifts.end());
        int mid_val = gifts[n / 2];
        int count = 0;
        for (const auto& e : gifts)
        {
            if (e == mid_val)
            {
                count++;
            }
        }
        if (count > n / 2)
            return mid_val;
        return 0;
    }
};