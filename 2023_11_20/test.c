#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
	printf("我是一个进程,pid:%d,ppid:%d\n",getpid(),getppid());
	int SubProcId=fork();
	if(SubProcId==0)
	{
		printf("我是子进程,pid:%d,ppid%d\n",getpid(),getppid());
	}	
	else if(SubProcId>0)
	{
		printf("我是父进程,pid:%d,ppid%d\n",getpid(),getppid());
	}
	return 0;
}
