#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef int HPDataType;

typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;

}HP;

void HeapInit(HP* php);
void Swap(HPDataType* p1, HPDataType* p2);
void AdjustUp(HPDataType* a, int child);
void HeapPush(HP* php, HPDataType x);
void AdjustDown(HPDataType* a, int n, int parent);
bool  HeapEmpty(HP* php);
int HeapSize(HP* php);
HPDataType HeapTop(HP* php);
void HeapPop(HP* php);
void HeapInitArray(HP* php, int* a, int n);
void HeapDestroy(HP* php);