#include "heap.h"
#define _CRT_SECURE_NO_WARNINGS 1
int main()
{
	HP heap;
	HeapInit(&heap);
	HeapPush(&heap, 0);
	printf("%d\n", HeapTop(&heap));
	HeapPop(&heap);
	HeapPush(&heap, 1);
	printf("%d\n", HeapTop(&heap));
	HeapPop(&heap);
	HeapPush(&heap, 2);
	printf("%d\n", HeapTop(&heap));
	HeapPop(&heap);
	HeapPush(&heap, 3);
	printf("%d\n", HeapTop(&heap));
	HeapPop(&heap);
	scanf("%d");
	return 0;
}