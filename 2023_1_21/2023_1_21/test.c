#define _CRT_SECURE_NO_WARNINGS 1



#include <stdio.h>
#include <string.h>
//int main()
//{
//	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[5] = { 0 };
//	int i = 0;
//	memcpy(arr2, arr1, 20);
//	for (i = 0;i < 5;i++)
//	{
//		printf("%d\n", arr2[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));
//	printf("%d\n", sizeof(a + 0));
//	printf("%d\n", sizeof(*a));
//	printf("%d\n", sizeof(a + 1));
//	printf("%d\n", sizeof(a[1]));
//	printf("%d\n", sizeof(&a));
//	printf("%d\n", sizeof(*&a));
//	printf("%d\n", sizeof(&a + 1));
//	printf("%d\n", sizeof(&a[0]));
//	printf("%d\n", sizeof(&a[0] + 1));
//	return 0;
//}

	////一维数组
	//int a[] = { 1,2,3,4 };//4*4=16
	//printf("%d\n", sizeof(a));//16
	//printf("%d\n", sizeof(a + 0));//a+0 其实是数组第一个元素的地址，是地址就是4/8字节
	//printf("%d\n", sizeof(*a));//*a是数组首元素，计算的是数组首元素的大小，单位是字节，4
	//printf("%d\n", sizeof(a + 1));//a+1是第二个元素的地址，是地址大小就是4/8
	//printf("%d\n", sizeof(a[1]));//a[1]是第二个元素，计算的是第二个元素的大小-4-单位是字节
	//printf("%d\n", sizeof(&a));//&a是整个数组的地址，整个数组的地址也是地址，地址的大小就是4/8字节
	////&a---> 类型：int(*)[4]
	//printf("%d\n", sizeof(*&a));//&a是数组的地址，*&a就是拿到了数组，*&a--> a,a就是数组名，sizeof(*&a)-->sizeof(a)
	////计算的是整个数组的大小，单位是字节-16
	//printf("%d\n", sizeof(&a + 1));//&a是整个数组的地址，&a+1，跳过整个数组，指向数组后边的空间，是一个地址，大小是4/8字节
	//printf("%d\n", sizeof(&a[0]));//&a[0]是首元素的地址，计算的是首元素地址的大小，4/8字节
	//printf("%d\n", sizeof(&a[0] + 1));//&a[0] + 1是第二个元素的地址，地址的大小就是4/8字节
//int main()
//{
	//char arr[] = { 'a','b','c','d','e','f' };
	//printf("%d\n", sizeof(arr));
	//printf("%d\n", sizeof(arr + 0));
	//printf("%d\n", sizeof(*arr));
	//printf("%d\n", sizeof(arr[1]));
	//printf("%d\n", sizeof(&arr));
	//printf("%d\n", sizeof(&arr + 1));
	//printf("%d\n", sizeof(&arr[0] + 1));
	//printf("%d\n", strlen(arr));
	//printf("%d\n", strlen(arr + 0));
	//printf("%d\n", strlen(*arr));
	//printf("%d\n", strlen(arr[1]));
	//printf("%d\n", strlen(&arr));
	//printf("%d\n", strlen(&arr + 1));
	//printf("%d\n", strlen(&arr[0] + 1));
	//char arr[] = "abcdef";
	//printf("%d\n", sizeof(arr));
	//printf("%d\n", sizeof(arr + 0));
	//printf("%d\n", sizeof(*arr));
	//printf("%d\n", sizeof(arr[1]));
	//printf("%d\n", sizeof(&arr));
	//printf("%d\n", sizeof(&arr + 1));
	//printf("%d\n", sizeof(&arr[0] + 1));
	//printf("%d\n", strlen(arr));
	//printf("%d\n", strlen(arr + 0));
	//printf("%d\n", strlen(*arr));
	//printf("%d\n", strlen(arr[1]));
	//printf("%d\n", strlen(&arr));
	//printf("%d\n", strlen(&arr + 1));
	//printf("%d\n", strlen(&arr[0] + 1));
	//char* p = "abcdef";
	//printf("%d\n", sizeof(p));
	//printf("%d\n", sizeof(p + 1));
	//printf("%d\n", sizeof(*p));
	//printf("%d\n", sizeof(p[0]));
	//printf("%d\n", sizeof(&p));
	//printf("%d\n", sizeof(&p + 1));
	//printf("%d\n", sizeof(&p[0] + 1));
	//printf("%d\n", strlen(p));
	//printf("%d\n", strlen(p + 1));
	//printf("%d\n", strlen(*p));
	//printf("%d\n", strlen(p[0]));
	//printf("%d\n", strlen(&p));
	//printf("%d\n", strlen(&p + 1));
	//printf("%d\n", strlen(&p[0] + 1));
//	return 0;
//}


	////字符数组
	//char arr[] = { 'a','b','c','d','e','f' };
	////char*
	////char [6]
	//printf("%d\n", strlen(arr));//随机值
	//printf("%d\n", strlen(arr + 0));//随机值
	//printf("%d\n", strlen(*arr));//strlen('a')->strlen(97),非法访问-err
	//printf("%d\n", strlen(arr[1]));//'b'-98,和上面的代码类似，是非法访问 - err
	//printf("%d\n", strlen(&arr));//&arr虽然是数组的地址，但是也是从数组起始位置开始的，计算的还是随机值
	////char(*)[6]
	//printf("%d\n", strlen(&arr + 1));
	////&arr是数组的地址，&arr+1是跳过整个数组的地址，求字符串长度也是随机值
	//printf("%d\n", strlen(&arr[0] + 1));//&arr[0] + 1是第二个元素的地址，是'b'的地址，求字符串长度也是随机值
	//printf("%d\n", sizeof(arr));//arr单独放在sizeof内部，计算的是整个数组的大小，单位是字节，6
	//printf("%d\n", sizeof(arr + 0));//arr + 0是数组首元素的地址，4/8
	//printf("%d\n", sizeof(*arr));//*arr是数组的首元素，计算的是首元素的大小-1字节
	//printf("%d\n", sizeof(arr[1]));//arr[1]是第二个元素，大小1字节
	//printf("%d\n", sizeof(&arr));//取出的数组的地址，数组的地址也是地址，是地址大小就是4/8
	//printf("%d\n", sizeof(&arr + 1));//&arr+1是跳过整个，指向数组后边空间的地址，4/8
	//printf("%d\n", sizeof(&arr[0] + 1));//&arr[0] + 1是数组第二个元素的地址，是地址4/8字节


	//char arr[] = "abcdef";//数组是7个元素
	////[a b c d e f \0]
	//printf("%d\n", strlen(arr));//6,arr是数组首元素的地址，strlen从首元素的地址开始统计\0之前出现的字符个数，是6
	//printf("%d\n", strlen(arr + 0));//arr + 0是数组首元素的地址，同第一个，结果是6
	//printf("%d\n", strlen(*arr));//*arr是'a',是97，传给strlen是一个非法的地址，造成非法访问
	//printf("%d\n", strlen(arr[1]));//err
	//printf("%d\n", strlen(&arr));//6
	//printf("%d\n", strlen(&arr + 1));//&arr + 1是跳过数组后的地址，统计字符串的长度是随机值
	//printf("%d\n", strlen(&arr[0] + 1));//&arr[0]+1是b的地址，从第二个字符往后统计字符串的长度，大小是5
	//printf("%d\n", sizeof(arr));//7 - 数组名单独放在sizeof内部，计算的是数组的总大小，单位是字节
	//printf("%d\n", sizeof(arr + 0));//arr+0是首元素的地址，大小是4/8
	//printf("%d\n", sizeof(*arr));//*arr是数组首元素，大小是1字节
	//printf("%d\n", sizeof(arr[1]));//arr[1]是数组的第二个元素，大小是1字节
	//printf("%d\n", sizeof(&arr));//&arr是数组的地址，数组的地址也是地址，是4/8字节
	//printf("%d\n", sizeof(&arr + 1));//&arr + 1是跳过整个数组的地址，是4/8字节
	//printf("%d\n", sizeof(&arr[0] + 1));//&arr[0] + 1是第二个元素的地址，是4/8字节

	//const char* p = "abcdef";
	//printf("%d\n", strlen(p));//6- 求字符串长度
	//printf("%d\n", strlen(p + 1));//p + 1是b的地址，求字符串长度就是5
	//printf("%d\n", strlen(*p));//err，*p是'a'
	//printf("%d\n", strlen(p[0]));//err - 同上一个
	//printf("%d\n", strlen(&p));//&p拿到的是p这个指针变量的起始地址，从这里开始求字符串长度完全是随机值
	//printf("%d\n", strlen(&p + 1));//&p+1是跳过p变量的地址，从这里开始求字符串长度也是随机值
	//printf("%d\n", strlen(&p[0] + 1));//&p[0] + 1是b的地址，从b的地址向后数字符串的长度是5
	//printf("%d\n", sizeof(p));//p是指针变量，大小就是4/8字节
	//printf("%d\n", sizeof(p + 1));//p + 1是b的地址，是地址，就是4/8个字节
	//printf("%d\n", sizeof(*p));//*p是'a'，sizeof(*p)计算的是字符的大小，是1字节
	//printf("%d\n", sizeof(p[0]));//p[0]-->*(p+0) --> *p  就同上一个，1字节
	//printf("%d\n", sizeof(&p));//&p是二级指针，是指针大小就是4/8
	//printf("%d\n", sizeof(&p + 1)); //&p + 1是跳过p变量后的地址，4/8字节
	//printf("%d\n", sizeof(&p[0] + 1));//p[0]就是‘a’,&p[0]就是a的地址，+1，就是b的地址，是地址就是4/8

	//	//二维数组
	//int a[3][4] = { 0 };
	//printf("%p\n", &a[0][0]);
	//printf("%p\n", a[0]+1);
	//printf("%d\n", sizeof(a));//48 = 3*4*4
	//printf("%d\n", sizeof(a[0][0]));//4
	//printf("%d\n", sizeof(a[0]));//a[0]是第一行的数组名，数组名单独放在sizeof内部，计算的就是数组(第一行)的大小，16个字节
	//printf("%d\n", sizeof(a[0] + 1));//a[0]作为第一行的数组名，没有单独放在sizeof内部，没有取地址，表示的就是数组首元素的地址
	////那就是a[0][0]的地址，a[0]+1就是第一行第二个元素的地址，是地址就是4/8个字节
	//printf("%d\n", sizeof(*(a[0] + 1)));//*(a[0] + 1)是第一行第2个元素，计算的是元素的大小-4个字节
	//printf("%d\n", sizeof(a + 1));//a是二维数组的数组名，数组名表示首元素的地址，就是第一行的地址，a+1就是第二行的地址
	////第二行的地址也是地址，是地址就是4/8   
	////a - int (*)[4]
	////a+1--> int(*)[4]
	//printf("%d\n", sizeof(*(a + 1)));//a+1是第二行的地址，*(a+1)表示的就是第二行，*(a+1)--a[1]  //16
	//printf("%d\n", sizeof(&a[0] + 1));//&a[0]是第一行的地址，&a[0]+1是第二行的地址，地址的大小就是4/8
	//printf("%d\n", sizeof(*(&a[0] + 1)));//*(&a[0] + 1) 是对第二行的地址解引用，得到的就是第二行，计算的就是第二行的大小
	//printf("%d\n", sizeof(*a));//a表示首元素的地址，就是第一行的地址，*a就是第一行，计算的就是第一行的大小
	////*a -- *(a+0)--a[0]
	//printf("%d\n", sizeof(a[3]));//16字节 int[4]
	////如果数组存在第四行，a[3]就是第四行的数组名，数组名单独放在sizeof内部，计算的是第四行的大小

int main()
{
	int a[3][4] = { 0 };
	printf("%d\n", sizeof(a));
	printf("%d\n", sizeof(a[0][0]));
	printf("%d\n", sizeof(a[0]));
	printf("%d\n", sizeof(a[0] + 1));
	printf("%d\n", sizeof(*(a[0] + 1)));
	printf("%d\n", sizeof(a + 1));
	printf("%d\n", sizeof(*(a + 1)));
	printf("%d\n", sizeof(&a[0] + 1));
	printf("%d\n", sizeof(*(&a[0] + 1)));
	printf("%d\n", sizeof(*a));
	printf("%d\n", sizeof(a[3]));
	return 0;
}