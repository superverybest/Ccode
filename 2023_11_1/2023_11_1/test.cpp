#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
#include <functional>

using namespace std;

double plus0(int a, int b, double rate)
{
	return (a + b) * rate;
}

string plus1(int a, double b)
{
	return std::to_string(a) + to_string(b)+"string";
}

int main()
{
	function<double(int, int)> res0 = bind(plus0,placeholders::_1,placeholders::_2,4.0);
	cout << res0(10, 5) << endl;
	function<string(int, double)>res1 = bind(plus1, placeholders::_1, placeholders::_2);
	cout << res1(100, 20) << endl;
}
