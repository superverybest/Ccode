
#include<iostream>

#include"string.h"

using namespace std;

void test_string1()
{
	superverybest::string s1("hello world");
	superverybest::string s2;
}

void test_string2()
{
	superverybest::string s1("hello world");
	cout << s1.c_str() << endl;
	superverybest::string s2;
	cout << s2.c_str() << endl;
}

void test_operator()
{
	superverybest::string s1("hello world");

	for (int i = 0;i < s1.size();i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;
}
void test_in_opeartor()
{
	superverybest::string s1("hello world");

	for (int i = 0;i < s1.size();i++)
	{
		s1[i]++;
	}
	cout << s1.c_str()<<endl;

}

void test_iterator()
{
	superverybest::string s1("hello world");
	superverybest::string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test_pushback()
{
	superverybest::string s1("hello world");
	s1.push_back(' ');
	s1.push_back('m');
	s1.push_back('n');
	cout << s1.c_str() << endl;
}

void test_append()
{
	superverybest::string s1("hello world");
	s1.append(".\nwhere the world will get to?");
	cout << s1.c_str() << endl;
}

void test_plusis()
{
	superverybest::string s1("hello world");
	s1 += ".\nwhere the world will get to";
	s1 += '?';
	cout << s1.c_str() << endl;
}

void test_insert()
{
	superverybest::string s1("hello world");
	s1.insert(6, "this ");
	cout << s1.c_str() << endl;
	s1.insert(0, 3,'*');
	cout << s1.c_str() << endl;
	s1.insert(3, 1, ' ');
	cout << s1.c_str() << endl;
}

void test_erase()
{
	superverybest::string s1("hello world");
	s1.erase(0, 6);
	cout << s1.c_str() << endl;
}

void test_find()
{
	superverybest::string s1("hello world");
	size_t a=s1.find('o');
	size_t b = s1.find("world");
	cout << a<< endl;
	cout << b << endl;
}

void test_substr()
{
	superverybest::string s1("hello world");
	superverybest::string res=s1.substr(3, 6);
	cout << res.c_str() << endl;
}

void test_string5()
{
	// 2118
	superverybest::string url = "ftp://www.baidu.com/?tn=65081411_1_oem_dg";

	size_t pos1 = url.find("://");
	if (pos1 != superverybest::string::npos)
	{
		superverybest::string protocol = url.substr(0, pos1);
		cout << protocol.c_str() << endl;
	}

	size_t pos2 = url.find('/', pos1 + 3);
	if (pos2 != superverybest::string::npos)
	{
		superverybest::string domain = url.substr(pos1 + 3, pos2 - (pos1 + 3));
		superverybest::string uri = url.substr(pos2 + 1);

		cout << domain.c_str() << endl;
		cout << uri.c_str() << endl;
	}
}

int main()
{
	//test_string1();
	//test_string2();
	//test_operator();
	//test_in_opeartor();
	//test_iterator();
	//test_pushback();
	//test_append();
	//test_plusis();
	//test_insert();
	//test_erase();
	//test_find();
	//test_substr();
	test_string5();
	return 0;
}
