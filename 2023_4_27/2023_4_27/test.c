#include "Queue.h"

int main()
{

	Queue q;
	QueueInit(&q);
	QueuePush(&q, 10);
	QueuePush(&q, 9);
	QueuePush(&q, 8);
	QueuePush(&q, 7);
	QueuePush(&q, 6);
	QueuePush(&q, 5);
	while (!QueueEmpty(&q))
	{
		printf("%d ",QueueFront(&q));
		QueuePop(&q);
	}
	printf("\n");

	return 0;
}

