#define _CRT_SECURE_NO_WARNINGS 1

//using namespace std;
//#include <iostream>
//#define N 10
//
//template <typename T>
//class Stack
//{
//	//......
//private:
//	T _a[N];
//	int _top;
//};
//
//int main()
//{
//	Stack<int> st1;
//	Stack<int> st2;
//
//	return 0;
//}

using namespace std;
#include <iostream>

template <typename T,size_t N>
class Stack
{
	//......
private:
	T _a[N];
	int _top;
};

int main()
{

	Stack<int,10> st1;
	Stack<int,100> st2;

	return 0;
}