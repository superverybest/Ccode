#include <stdio.h> 
#include <math.h> 
int main()
{
	int x=0;
	float res=0;
	scanf("%d",&x);
	if(x<-1)
	{
		res=2*x-1;
	}
	else if(x>=-1&&x<0)
	{
		res=(2/3)*x;
	}
	else if(x==0)
	{
		res=0;
	}
	else if(x>0)
	{
		res=x*x+8+sqrt(x);
	}
	printf("%.2f",res);
	return 0;
}
