//#include <stdio.h>
//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int f_arr[10000] = { 0,1,2 };
//    for (int i = 3;i < 10001;i++)
//    {
//        f_arr[i] = f_arr[i - 1] + f_arr[i - 2];
//        f_arr[i] %= 10000;
//    }
//
//    int n = 0;
//    while (cin >> n)
//    {
//        for (int i = 0;i < n;i++)
//        {
//            int num;
//            cin >> num;
//            printf("%04d", f_arr[num]);
//        }
//        printf("\n");
//    }
//    return 0;
//}

//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param number int整型
//     * @return int整型
//     */
//    int jumpFloorII(int number)
//    {
//        if (number == 1)
//        {
//            return 1;
//        }
//        else if (number >= 2)
//        {
//            return 2 * jumpFloorII(number - 1);
//        }
//        return 0;
//    }
//};

#include <iostream>
using namespace std;

int main()
{
    float n = 0;
    float r = 0;
    while (cin >> n >> r)
    {
        if (n > 2 * r * 3.14)
            cout << "No" << endl;
        else if (n <= 2 * r * 3.14)
            cout << "Yes" << endl;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")