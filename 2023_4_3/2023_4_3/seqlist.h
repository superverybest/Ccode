#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef int SLTDataType;

typedef struct SLTNode
{
	SLTDataType Data;
	struct SLTNode* Next;
}SLTNode;
