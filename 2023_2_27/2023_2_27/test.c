#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
#include <string.h>

void Load(struct Players* ps);
void Save(struct Players* ps);

typedef struct PlayInform
{
	int points;
	char name[20];

}PlayInform;

typedef struct Players
{

	struct PlayInform data[500];
	
	int size;

}Players;

int Find(struct Players* ps, char player_name[20])
{
	int i = 0;
	for (i = 0;i < ps->size;i++)
	{
		if (strcmp(ps->data[i].name, player_name) == 0)
		{
			return i;
		}
	}
		return -1;
}

void Store(int points,struct Players *ps)
{
	int ret = 0;

	printf("请输入玩家姓名用于保存游戏记录\n");

	char name[20];

	scanf("%s", &name);

	ret = Find(ps, name);
	if (ret == -1)
	{
		ps->data[ps->size].points = points;
		strcpy(ps->data[ps->size].name, name);
		ps->size++;
	}
	else
	{
		ps->data[ret].points += points;
	}

	Save(ps);
}

void Show(struct Players * ps,struct Players * update)
{

	int i = 0;
	for (i = 0;i < 500;i++)
	{
		update->data[i].points = 0;
	}
	update->size = 0;
	Load(update);

	if (ps->size == 0)
	{
		printf("玩家信息为空\n");
	}
	else
	{
		int i = 0;
		int j = 0;

		PlayInform sort;

		for (i = 0;i < ps->size - 1; i++)
		{
			for (j = i;j < ps->size - 1;j++)
			{
				if (ps->data[j].points < ps->data[j + 1].points)
				{
					sort = ps->data[j];
					ps->data[j] = ps->data[j + 1];
					ps->data[j + 1] = sort;
				}
			}
		}
		for (i = 0;i < ps->size;i++)
		{
			printf("%d %s\n", ps->data[i].points,ps->data[i].name);
		}
	}
}

void Load(struct Players * ps)
{
	PlayInform tmp = { 0 };
	FILE* PfRead = fopen("players.dat", "rb");
	if (PfRead == NULL)
	{
		printf("Load:%s\n", strerror(errno));
		return;
	}

	while (fread(&tmp, sizeof(PlayInform), 1, PfRead))
	{
		ps->data[ps->size] = tmp;
		ps->size++;
	}
	fclose(PfRead);
	PfRead = NULL;
}

void Save(struct Players* ps)
{
	FILE* pfWrite = fopen("players.dat", "wb");
	if (pfWrite == NULL)
	{
		printf("Save:%s", strerror(errno));
		return;
	}
	int i = 0;
	for (i = 0;i < ps->size;i++)
	{
		fwrite(&(ps->data[i]), sizeof(PlayInform), 1, pfWrite);
	}

	fclose(pfWrite);
	pfWrite = NULL;
}

int main()
{
	struct Players players;
	int i = 0;
	for (i = 0;i < 500;i++)
	{
		players.data[i].points = 0;
	}
	players.size = 0;
	Load(&players);

	int point = 0;
	scanf("%d", &point);
	Store(point, &players);

	Show(&players,&players);

	//判断玩家胜负后
	//记录分数
	//然后调用Store
	//把记录分数的变量传到Store中
	//进入Store中记录玩家姓名
	//Store里有Save，所以不用再保存了
	//
	//显示玩家排名时
	//调用Show
	//Show里有初始化和Load
	//所以能够及时更新数据
	//进入函数后则进行排名和打印
	//

	return 0;
}